package controller;

import controller.body.BodyImpl;
import controller.body.BodyInterface;
import controller.color.ColorImpl;
import controller.color.ColorInterface;
import controller.fueltype.FuelTypeImpl;
import controller.fueltype.FuelTypeInterface;
import controller.model.ModelImpl;
import controller.model.ModelInterface;
import controller.textile.TextileImpl;
import controller.textile.TextileInterface;
import model.*;
import view.printerwrapper.PrinterWrapper;


public class CarSalonControllerImpl implements CarSalonMVC.Controller{

        private ColorInterface colorInterface;
        private BodyInterface bodyInterface;
        private ModelInterface modelInterface;
        private FuelTypeInterface fuelTypeInterface;
        private TextileInterface textileInterface;
        private PrinterWrapper printerWrapper;
        private ScannerWrapper scannerWrapper;
        private Car car;
        private Wallet wallet;
        private CarSalonMVC.View view;

        public CarSalonControllerImpl(ScannerWrapper scannerWrapper, Wallet wallet, CarSalonMVC.View view, PrinterWrapper printerWrapper) {
            this.view = view;
            this.scannerWrapper = scannerWrapper;
            this.wallet = wallet;
            this.printerWrapper = printerWrapper;
            this.colorInterface = new ColorImpl(scannerWrapper,printerWrapper);
            this.modelInterface = new ModelImpl(scannerWrapper, printerWrapper);
            this.textileInterface = new TextileImpl(scannerWrapper, printerWrapper);
            this.fuelTypeInterface = new FuelTypeImpl(scannerWrapper, printerWrapper);
            this.bodyInterface = new BodyImpl(scannerWrapper,printerWrapper);
        }

        public void start() {
            car = new Car();
        }

        public void setAmountOfMoney(){
            int money =-1;
            while(true) {
                try {
                    String moneyToParse = scannerWrapper.nextLine();
                    money = Integer.parseInt(moneyToParse);
                    break;
                } catch (Exception exception) {
                    printerWrapper.display("Wrong data! Please, try again:");
                }
            }
            wallet.setMoney(money);
        }


    public Car getCar() {
        return car;
    }

    public String finish(){
        return car.getModel() + " " + car.getBody() + " " + car.getColor() + " " + car.getFuelType() + " " + car.getTextile();
    }

    public void detach() {
        view=null;
    }

    @Override
    public boolean setColor() {
            car.setColor(colorInterface.chooseColor(wallet));
            if(car.getColor()!=null)wallet.setMoney(wallet.getMoney()-car.getColor().getPrice());
            return car.getColor() != null;
    }

    @Override
    public boolean setBody() {
            car.setBody(bodyInterface.chooseBody(wallet));
        if(car.getBody()!=null)wallet.setMoney(wallet.getMoney()-car.getBody().getPrice());
        return car.getBody() != null;
    }

    @Override
    public boolean setFuelType() {
            car.setFuelType(fuelTypeInterface.chooseFuelType(wallet));
        if(car.getFuelType()!=null)wallet.setMoney(wallet.getMoney()-car.getFuelType().getPrice());
        return car.getFuelType() != null;
    }

    @Override
    public boolean setTextile() {

            car.setTextile(textileInterface.chooseTextile(wallet));
        if(car.getTextile()!=null)wallet.setMoney(wallet.getMoney()-car.getTextile().getPrice());
        return car.getTextile() != null;
    }

    @Override
    public boolean setModel() {
            car.setModel(modelInterface.chooseModel(wallet));
        if(car.getModel()!=null)wallet.setMoney(wallet.getMoney()-car.getModel().getPrice());
        return car.getModel() != null;}
}


