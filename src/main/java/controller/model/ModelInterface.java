package controller.model;

import controller.Wallet;
import model.carparameters.Model;

public interface ModelInterface {
    Model chooseModel(Wallet wallet);
}
