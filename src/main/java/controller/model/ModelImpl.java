package controller.model;

import controller.ScannerWrapper;
import controller.Wallet;
import model.carparameters.Body;
import model.carparameters.Model;
import view.printerwrapper.PrinterWrapper;

public class ModelImpl implements ModelInterface {

    private ScannerWrapper scannerWrapper;
    private PrinterWrapper printerWrapper;

    public ModelImpl(ScannerWrapper scannerWrapper, PrinterWrapper printerWrapper) {
        this.scannerWrapper = scannerWrapper;
        this.printerWrapper = printerWrapper;
    }

    public Model chooseModel(Wallet wallet) {
        int option =-2;
        while(true) {
            String optionToParse = scannerWrapper.nextLine();
            option = Model.containsIndex(optionToParse);
            if(option==-1){
                printerWrapper.display("Wrong data! Please, try again:");
            } else {break;}
        }

        switch (option) {
            case 1:
                if(Model.COROLLA.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return Model.COROLLA; }
            case 2:
                if(Model.CAMRY.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return Model.CAMRY; }
            case 0:
                return null;
        }
        return null;
    }

}
