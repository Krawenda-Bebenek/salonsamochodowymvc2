package controller.fueltype;

import controller.ScannerWrapper;
import controller.Wallet;
import model.carparameters.Body;
import model.carparameters.FuelType;
import model.carparameters.Model;
import view.printerwrapper.PrinterWrapper;

public class FuelTypeImpl implements FuelTypeInterface{
    private ScannerWrapper scannerWrapper;
    private PrinterWrapper printerWrapper;

    public FuelTypeImpl(ScannerWrapper scannerWrapper, PrinterWrapper printerWrapper) {
        this.scannerWrapper = scannerWrapper;
        this.printerWrapper = printerWrapper;
    }

    @Override
    public FuelType chooseFuelType(Wallet wallet) {
        int option =-2;
        while(true) {
            String optionToParse = scannerWrapper.nextLine();
            option = FuelType.containsIndex(optionToParse);
            if(option==-1){
                printerWrapper.display("Wrong data! Please, try again:");
            } else {break;}
        }
        switch (option) {
            case 1:
                if(FuelType.DIESEL_FUEL.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return FuelType.DIESEL_FUEL; }
            case 2:
                if(FuelType.PETROL_FUEL.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return FuelType.PETROL_FUEL; }
            case 0:
                return null;
        }
        return null;
    }
}
