package controller.fueltype;

import controller.Wallet;
import model.carparameters.FuelType;

public interface FuelTypeInterface {
    FuelType chooseFuelType(Wallet wallet);
}
