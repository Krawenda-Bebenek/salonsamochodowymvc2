package controller.color;

import controller.ScannerWrapper;
import controller.Wallet;
import model.carparameters.Body;
import model.carparameters.Color;
import model.carparameters.Model;
import view.printerwrapper.PrinterWrapper;

public class ColorImpl implements ColorInterface {

    private ScannerWrapper scannerWrapper;
    private PrinterWrapper printerWrapper;

    public ColorImpl(ScannerWrapper scannerWrapper, PrinterWrapper printerWrapper) {
        this.scannerWrapper = scannerWrapper;
        this.printerWrapper = printerWrapper;
    }

    public Color chooseColor(Wallet wallet) {
        int option =-2;
        while(true) {
            String optionToParse = scannerWrapper.nextLine();
            option = Color.containsIndex(optionToParse);
            if(option==-1){
                printerWrapper.display("Wrong data! Please, try again:");
            } else {break;}
        }
        switch (option) {
            case 1:
                if(Color.RED.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return Color.RED; }
            case 2:
                if(Color.GREEN.getPrice() > wallet.getMoney()){
                   throw new IllegalStateException();
                } else {
                    return  Color.GREEN;
                }
            case 0:
                return null;
        }
        return null;
    }
}
