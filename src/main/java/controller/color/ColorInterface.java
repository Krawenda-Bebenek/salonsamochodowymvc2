package controller.color;

import controller.Wallet;
import model.carparameters.Color;

public interface ColorInterface {
     Color chooseColor(Wallet wallet);
}
