package controller;

import java.util.Scanner;

public class ScannerWrapperImpl implements ScannerWrapper{

    public String nextLine() {
        return new Scanner(System.in).nextLine();
    }
}
