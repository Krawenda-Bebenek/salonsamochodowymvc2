package controller.body;

import controller.ScannerWrapper;
import controller.Wallet;
import controller.body.BodyInterface;
import model.carparameters.Body;
import model.carparameters.Model;
import view.printerwrapper.PrinterWrapper;

public class BodyImpl implements BodyInterface {

    private ScannerWrapper scannerWrapper;
    private PrinterWrapper printerWrapper;

    public BodyImpl(ScannerWrapper scannerWrapper, PrinterWrapper printerWrapper) {
        this.scannerWrapper = scannerWrapper;
        this.printerWrapper = printerWrapper;
    }

    @Override
    public Body chooseBody(Wallet wallet) {
        int option =-2;
        while(true) {
            String optionToParse = scannerWrapper.nextLine();
            option = Body.containsIndex(optionToParse);
            if(option==-1){
                printerWrapper.display("Wrong data! Please, try again:");
            } else {break;}
        }
        switch (option) {
            case 1:
                if(Body.SEDAN.getPrice() > wallet.getMoney()){
                throw new IllegalStateException();
            } else {
                return Body.SEDAN; }
            case 2:
                if(Body.COMBI.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return Body.COMBI; }
            case 0:
                return null;
        }
        return null;
    }
}
