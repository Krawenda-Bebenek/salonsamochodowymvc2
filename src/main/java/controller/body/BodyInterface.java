package controller.body;

import controller.Wallet;
import model.carparameters.Body;

public interface BodyInterface {
    Body chooseBody(Wallet wallet);
}
