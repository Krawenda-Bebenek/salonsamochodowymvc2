package controller.textile;

import controller.ScannerWrapper;
import controller.Wallet;
import model.carparameters.Body;
import model.carparameters.Model;
import model.carparameters.Textile;
import view.printerwrapper.PrinterWrapper;

public class TextileImpl implements TextileInterface {
    private ScannerWrapper scannerWrapper;
    private PrinterWrapper printerWrapper;

    public TextileImpl(ScannerWrapper scannerWrapper, PrinterWrapper printerWrapper) {
        this.scannerWrapper = scannerWrapper;
        this.printerWrapper = printerWrapper;
    }
    public Textile chooseTextile(Wallet wallet) {
        int option =-2;
        while(true) {
            String optionToParse = scannerWrapper.nextLine();
            option = Textile.containsIndex(optionToParse);
            if(option==-1){
                printerWrapper.display("Wrong data! Please, try again:");
            } else {break;}
        }
        switch (option) {
            case 1:
                if(Textile.VELOUR.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return Textile.VELOUR; }
            case 2:
                if(Textile.LEATHER.getPrice() > wallet.getMoney()){
                    throw new IllegalStateException();
                } else {
                    return Textile.LEATHER; }
            case 0:
                return null;
        }
        return null;
    }
}
