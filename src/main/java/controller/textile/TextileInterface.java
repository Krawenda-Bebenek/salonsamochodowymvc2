package controller.textile;

import controller.Wallet;
import model.carparameters.Textile;

public interface TextileInterface {
    Textile chooseTextile(Wallet wallet);
}
