package controller;

public interface ScannerWrapper {

    String nextLine();
}
