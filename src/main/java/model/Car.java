package model;

import model.carparameters.*;

public class Car {
    private Color color;
    private Model model;
    private FuelType fuelType;
    private Textile textile;
    private Body body;

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public Textile getTextile() {
        return textile;
    }

    public void setTextile(Textile textile) {
        this.textile = textile;
    }

    @Override
    public String toString() {
        return model + " " + body + " " + color + " " + model + " " + fuelType + " " + textile;
    }
}
