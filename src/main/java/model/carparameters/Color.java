package model.carparameters;

public enum Color {
    RED("red", 250, 1),
    GREEN("green", 2000,2);
//    BLACK("black", 0, 3),
//   WHITE("white", 0,4),
//   BLUE("blue", 0, 5),
// GOLDEN("golden", 2000,6),
// SILVER("silver", 1000,7);

    private String name;
    private int price;
    private int index;

    Color(String name, int price, int index) {
        this.name = name;
        this.price = price;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }


    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "Index: " + index + ", " + name + ", price: " + price;
    }

    public static String displayColorOptions(){
        StringBuilder stringBuilder = new StringBuilder();
        for(Color color : values()){
            stringBuilder.append(color.toString()+"\n");
        }
        return stringBuilder.toString();
    }

    public static int containsIndex(String index){
        Integer parsedIndex;
        try {
            parsedIndex = Integer.parseInt(index);
        } catch (Exception exception){
            return -1;
        }
        if(parsedIndex==0)return 0;
        for(Color body : values()){
            if(body.index==parsedIndex){
                return parsedIndex;
            }
        }
        return -1;
    }
}