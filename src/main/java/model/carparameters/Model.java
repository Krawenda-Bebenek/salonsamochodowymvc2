package model.carparameters;

public enum Model {

        COROLLA(75900, "Corolla", 1),
        CAMRY(141900, "Camry", 2);
//        AURIS(71900, "Auris", 3),
//        YARIS(43500, "Yaris", 4),
//        HILUX(140500, "Hilux", 5);

        private int price;
        private String modelCar;
        private int index;

        Model(int priceModel, String modelCar, int index) {
            this.price = priceModel;
            this.modelCar = modelCar;
            this.index = index;
        }

        public int getPrice() {
            return price;
        }

        public String getModelCar() {
            return modelCar;
        }

        public int getIndex() {
            return index;
        }

        @Override
        public String toString() {
            return "Index: " + index + ", " + modelCar + ", price: " + price;
        }

    public static String displayModelOptions(){
        StringBuilder stringBuilder = new StringBuilder();
        for(Model model : values()){
            stringBuilder.append(model.toString()+"\n");
        }
        return stringBuilder.toString();
    }
    public static int containsIndex(String index){
        Integer parsedIndex;
        try {
            parsedIndex = Integer.parseInt(index);
        } catch (Exception exception){
            return -1;
        }
        for(Model body : values()){
            if(body.index==parsedIndex){
                return parsedIndex;
            }
        }
        return -1;
    }

}
