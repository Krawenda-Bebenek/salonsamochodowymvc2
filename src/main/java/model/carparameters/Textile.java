package model.carparameters;

public enum Textile {
    VELOUR("Velour", 0, 1),
    LEATHER("Leather", 1000, 2);
//    LEATHER_AND_QUILTED("Leather and quilted", 2000, 3);

    private String name;
    private int price;
    private int index;

    Textile(String name, int price, int index) {
        this.name = name;
        this.price = price;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return  index + ". " + name + " price: " + price;
    }

    public static String displayTextileOptions(){
        StringBuilder stringBuilder = new StringBuilder();
        for(Textile textile : values()){
            stringBuilder.append(textile.toString()+"\n");
        }
        return stringBuilder.toString();
    }

    public static int containsIndex(String index){
        Integer parsedIndex;
        try {
            parsedIndex = Integer.parseInt(index);
        } catch (Exception exception){
            return -1;
        }
        if(parsedIndex==0)return 0;
        for(Textile body : values()){
            if(body.index==parsedIndex){
                return parsedIndex;
            }
        }
        return -1;
    }
}