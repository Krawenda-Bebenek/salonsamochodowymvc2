package model.carparameters;

public enum FuelType {
    DIESEL_FUEL(10000, "Diesel", 1),
    PETROL_FUEL(0, "Petrol", 2);
//    HYBRID_FUEL(15000, "Hybrid", 3);

    private int price;
    private String name;
    private int index;

    FuelType(int priceFuel, String nameFuel, int index) {
        this.price = priceFuel;
        this.name = nameFuel;
        this.index = index;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "Index: " + index + ", " + name + ", price: " + price;
    }

    public static String displayFuelTypeOptions(){
        StringBuilder stringBuilder = new StringBuilder();
        for(FuelType fuelType : values()){
            stringBuilder.append(fuelType.toString()+"\n");
        }
        return stringBuilder.toString();
    }

    public static int containsIndex(String index){
        Integer parsedIndex;
        try {
            parsedIndex = Integer.parseInt(index);
        } catch (Exception exception){
            return -1;
        }
        if(parsedIndex==0)return 0;
        for(FuelType body : values()){
            if(body.index==parsedIndex){
                return parsedIndex;
            }
        }
        return -1;
    }
}