package model.carparameters;

public enum Body {
    SEDAN("Sedan", 0,1),
    COMBI("Combi", 2000,2);
//    HATCHBACK("Hatchback", 1000,3),
//    COMBI("Combi", 1000,4);

    private String type;
    private int price;
    private int index;

    Body(String type, int price, int index) {
        this.type = type;
        this.price = price;
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "Index: " + index + ", " + type + ", price: " + price;
    }

    public static String displayBodyOptions(){
        StringBuilder stringBuilder = new StringBuilder();
        for(Body body : values()){
            stringBuilder.append(body.toString()+"\n");
        }
        return stringBuilder.toString();
    }

   public static int containsIndex(String index){
        Integer parsedIndex;
        try {
            parsedIndex = Integer.parseInt(index);
        } catch (Exception exception){
            return -1;
        }
        if(parsedIndex==0)return 0;
        for(Body body : values()){
            if(body.index==parsedIndex){
                return parsedIndex;
            }
        }
        return -1;
    }
}
