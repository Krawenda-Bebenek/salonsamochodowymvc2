package model;

public interface CarSalonMVC {
    interface Controller {
        void start();
        void setAmountOfMoney();
        boolean setColor();
        boolean setBody();
        boolean setFuelType();
        boolean setTextile();
        boolean setModel();
        Car getCar();
        String finish();
        void detach();
    }

    interface View {
        void introduceAmountOfMoney();
        void introduceColor();
        void introduceModel();
        void introduceBody();
        void introduceFuelType();
        void introduceTextile();

    }
}
