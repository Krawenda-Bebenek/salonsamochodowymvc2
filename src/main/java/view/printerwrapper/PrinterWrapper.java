package view.printerwrapper;

public interface PrinterWrapper {
    void display(String text);
}
