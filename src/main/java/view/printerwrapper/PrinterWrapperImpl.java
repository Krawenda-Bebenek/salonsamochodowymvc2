package view.printerwrapper;

import view.printerwrapper.PrinterWrapper;

public class PrinterWrapperImpl implements PrinterWrapper {
    @Override
    public void display(String text) {
        System.out.println(text);
    }
}
