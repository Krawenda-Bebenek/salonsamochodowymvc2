package view;

import controller.CarSalonControllerImpl;
import controller.ScannerWrapperImpl;
import controller.Wallet;
import model.CarSalonMVC;
import view.printerwrapper.PrinterWrapperImpl;

public class Main {

    public static void main(String[] args) {
        CarSalonMVC.View view = new CarSalonViewImpl(new PrinterWrapperImpl());
        CarSalonMVC.Controller controller = new CarSalonControllerImpl(new ScannerWrapperImpl(), new Wallet(), view, new PrinterWrapperImpl());
        controller.start();

        view.introduceAmountOfMoney();
        controller.setAmountOfMoney();

        int state = 1;
        while (state != 6)
        {
            switch (state) {
                case 1:
                    view.introduceModel();
                    if(!controller.setModel()) {
                        break;
                    }
                case 2:
                    view.introduceBody();
                    if(!controller.setBody()) {
                        state = 1;
                        break;
                    }
                case 3:
                    view.introduceColor();
                    if(!controller.setColor()){
                        state=2;
                        break;
                    }
                case 4:
                    view.introduceFuelType();
                    if(!controller.setFuelType()){
                        state=3;
                        break;
                    }
                case 5:
                    view.introduceTextile();
                    if(!controller.setTextile()){
                        state=4;
                        break;
                    }
                    state = 6;
                    break;
                default:
                    break;
            }
        }
        System.out.println(controller.finish());
        controller.detach();
    }
}
