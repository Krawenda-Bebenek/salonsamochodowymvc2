package view;

import model.CarSalonMVC;
import model.carparameters.Body;
import model.carparameters.Color;
import model.carparameters.FuelType;
import model.carparameters.Model;
import model.carparameters.Textile;
import view.printerwrapper.PrinterWrapper;

public class CarSalonViewImpl implements CarSalonMVC.View {

    private PrinterWrapper printerWrapper;
    private CarSalonMVC.View view;

    public CarSalonViewImpl(PrinterWrapper printerWrapper) {
        this.printerWrapper = printerWrapper;
    }

    public void attach(CarSalonMVC.View view){
        this.view = view;
    }


    @Override
    public void introduceAmountOfMoney() {
       printerWrapper.display("Introduce amount of money:");
    }

    @Override
    public void introduceColor() {
        printerWrapper.display("Choose color:\n" +"Index: 0, Back\n" +  Color.displayColorOptions());
    }

    @Override
    public void introduceModel() {
        printerWrapper.display("Choose model:\n" + Model.displayModelOptions());
    }

    @Override
    public void introduceBody() {
        printerWrapper.display("Choose Body:\n"+"Index: 0, Back\n" + Body.displayBodyOptions());
    }

    @Override
    public void introduceFuelType() {
        printerWrapper.display("Choose type of fuel:\n"+"Index: 0, Back\n" + FuelType.displayFuelTypeOptions());
    }

    @Override
    public void introduceTextile() {
        printerWrapper.display("Choose textile:\n"+"Index: 0, Back\n" + Textile.displayTextileOptions());
    }
}
