package view;

import model.CarSalonMVC;
import org.junit.Test;
import org.mockito.Mockito;
import view.printerwrapper.PrinterWrapper;


public class CarSalonViewImplTest {
    PrinterWrapper printerWrapper = Mockito.mock(PrinterWrapper.class);
    CarSalonViewImpl tested = new CarSalonViewImpl (printerWrapper);
    CarSalonMVC.View viewMock = Mockito.mock(CarSalonMVC.View.class);

   @Test
   public void introduceAMountOfMoneyTest(){
       tested.introduceAmountOfMoney();
       Mockito.verify(printerWrapper).display("Introduce amount of money:");
   }

   @Test
    public void introduceTextileTest(){
       tested.introduceTextile();
       Mockito.verify(printerWrapper).display("Choose textile:\nIndex: 0, Back\n1. Velour price: 0\n2. Leather price: 1000\n"
              );
   }
}
