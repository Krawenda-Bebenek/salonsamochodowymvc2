package controller;

import controller.body.BodyImpl;
import controller.color.ColorImpl;
import controller.fueltype.FuelTypeImpl;
import controller.model.ModelImpl;
import controller.textile.TextileImpl;
import model.carparameters.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import view.CarSalonViewImpl;
import view.printerwrapper.PrinterWrapperImpl;

public class CarParameterControllerImplTest {

    ScannerWrapper scannerWrapper = Mockito.mock(ScannerWrapper.class);
    PrinterWrapperImpl printerWrapper = new PrinterWrapperImpl();
    Wallet wallet = Mockito.mock(Wallet.class);
    ColorImpl colorTest = new ColorImpl(scannerWrapper, printerWrapper );
    ModelImpl modelTest = new ModelImpl(scannerWrapper, printerWrapper);
    BodyImpl bodyTest = new BodyImpl(scannerWrapper, printerWrapper);
    FuelTypeImpl fuelTypeTest = new FuelTypeImpl(scannerWrapper, printerWrapper);
    TextileImpl textileTest = new TextileImpl(scannerWrapper, printerWrapper);

    //color1
    @Test
    public void chooseColorRedCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("1");
        Mockito.when(wallet.getMoney()).thenReturn(7777);
        Assertions.assertEquals(Color.RED, colorTest.chooseColor(wallet));
    }

    @Test(expected = IllegalStateException.class)
    public void chooseColorGreenCantAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(0);
        colorTest.chooseColor(wallet);
    }

    @Test
    public void chooseColorGreenCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(2000);
        Assertions.assertEquals(Color.GREEN, colorTest.chooseColor(wallet));
    }


    //body2
    @Test
    public void chooseSedanCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("1");
        Mockito.when(wallet.getMoney()).thenReturn(1);
        Assertions.assertEquals(Body.SEDAN, bodyTest.chooseBody(wallet));
    }

    @Test
    public void chooseCombiCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(10000);
        Assertions.assertEquals(Body.COMBI, bodyTest.chooseBody(wallet));
    }

    @Test(expected = IllegalStateException.class)
    public void chooseCombiCantAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(999);
        bodyTest.chooseBody(wallet);
    }


    //model3
    @Test
    public void chooseCorollaModelCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("1");
        Mockito.when(wallet.getMoney()).thenReturn(75900);
        Assertions.assertEquals(Model.COROLLA, modelTest.chooseModel(wallet));
    }

    @Test
    public void chooseCamryModelCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(141900);
        Assertions.assertEquals(Model.CAMRY, modelTest.chooseModel(wallet));
    }

    @Test(expected = IllegalStateException.class)
    public void chooseCamryModelCantAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(141899);
        modelTest.chooseModel(wallet);

    }

    @Test(expected = IllegalStateException.class)
    public void chooseCorollaModelCantAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(5899);
        modelTest.chooseModel(wallet);
    }


//textil4

    @Test
    public void chooseVelurTextileAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("1");
        Mockito.when(wallet.getMoney()).thenReturn(1);
        Assertions.assertEquals(Textile.VELOUR, textileTest.chooseTextile(wallet));
    }

    @Test
    public void chooseLeatherTextileAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(1000);
        Assertions.assertEquals(Textile.LEATHER, textileTest.chooseTextile(wallet));
    }

    @Test(expected = IllegalStateException.class)
    public void chooseLeatherTextileCantAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(900);
        textileTest.chooseTextile(wallet);
    }

    //fuel5
    @Test
    public void chooseFuelPetroldCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("2");
        Mockito.when(wallet.getMoney()).thenReturn(0);
        Assertions.assertEquals(FuelType.PETROL_FUEL, fuelTypeTest.chooseFuelType(wallet));
    }

    @Test
    public void chooseFuelDieselCanAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("1");
        Mockito.when(wallet.getMoney()).thenReturn(10000);
        Assertions.assertEquals(FuelType.DIESEL_FUEL, fuelTypeTest.chooseFuelType(wallet));
    }

    @Test(expected = IllegalStateException.class)
    public void chooseFuelDieselCantAfford() {
        Mockito.when(scannerWrapper.nextLine()).thenReturn("1");
        Mockito.when(wallet.getMoney()).thenReturn(9999);
        fuelTypeTest.chooseFuelType(wallet);
    }
}
